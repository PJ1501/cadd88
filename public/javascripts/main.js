//init
// window.onload = MM_preloadImages('images/menu/game_b_ro1.png','images/menu/game_b_ro2.png',
// 'images/menu/game_b_ro3.png','images/menu/game_b_ro4.png','images/menu/sub_menu_ro_02.png',
// 'images/menu/sub_menu_ro_03.png','images/menu/sub_menu_ro_04.png','images/menu/sub_menu_ro_05.png',
// 'images/menu/sub_menu_ro_06.png','images/menu/sub_menu_ro_07.png');

//get game infor news

function loadInforNews() {

    request(GAME_DADOL_NEW_URL).then(
        (response) => {
            if (response !== "LOGOUT") {
                let v = response.split("$");
                let nNowTime = parseInt(v[0]);
                let nEndime = parseInt(v[1]);
                let magam = 0;
                let chasu = parseInt(v[2]);
                let magam_time = parseInt(v[3]);
                let p_chasu = v[6];

            }
        },
        (error) => {
            console.error("something went wrong: " + error);
        })
}

function pushData(value) {
    return new Promise((resolve, reject) => {
        if (value === 0 || value === 1) {
            $.ajax({
                url: "/testdb",
                method: "POST",
                data: { result: value },
                success: function(data, status, jqXHR) {
                    if (jqXHR.status == 200) {
                        resolve(data);
                    } else {
                        console.log(jqXHR);
                        reject("can not finish!!");
                    }
                },
                error: function(error) {
                    reject(error);
                }
            })
        } else {
            reject("Value not valid");
        }
    })
}
let tabbar;
//init
window.onload = function() {

        const submitBtn0 = document.querySelector("#submit_result0");
        const submitBtn1 = document.querySelector("#submit_result1");

        const resultPage = document.querySelector("#result_page");
        const predictPage = document.querySelector("#predict_page");

        const resultContent = document.querySelector("#result_content");
        const predictContent = document.querySelector("#predict_content");
        //init mdc
        mdc.textField.MDCTextField.attachTo(document.querySelector('.mdc-text-field'));
        mdc.ripple.MDCRipple.attachTo(document.querySelector('.mdc-ripple'));
        tabbar = mdc.tabBar.MDCTabBar.attachTo(document.querySelector('.mdc-tab-bar'));

        tabbar.foundation_.adapter_.notifyTabActivated = function(tab) {
            if (tab == 0) {
                resultPage.style.display = "block";
                predictPage.style.display = "none";
            } else {
                resultPage.style.display = "none";
                predictPage.style.display = "block";
            }
        }


        function disableBtns(value) {
            if (value) {
                submitBtn0.setAttribute("disabled", true);
                submitBtn1.setAttribute("disabled", true);
            } else {
                submitBtn0.removeAttribute("disabled");
                submitBtn1.removeAttribute("disabled");
            }
        }
        const badges = ["badge-primary", "badge-secondary", "badge-success", "badge-danger", "badge-warning", "badge-info"];
        const alerts = ["alert-primary", "alert-secondary", "alert-success", "alert-danger", "alert-warning", "alert-info"]

        function makeBadges(value, max) {

            let records = [];
            for (let i = 0; i < max; i++) {
                if (i <= (max - value.length - 1)) {
                    records.push(`<span class="badge badge-secondary"> -</span>`)
                } else {
                    records.push(`<span class="badge ${badges[i%badges.length]}"> ${value[i-(max-value.length)]}</span>`)
                }
            }
            return records.join("");
            // if (value.length > 0) {
            //     for (let i = 0; i < result.results.length; i++) {
            //         records.push(`<span class="badge ${badges[i%badges.length]}"> ${result.results[i]}</span>`);
            //     }
            // }
        }

        function onSuccess(result) {
            let result8 = result.queryAll[0] ? `<li class="list-group-item"> <label>Result8: </label> ${makeBadges(result.queryAll[0],20)} </li>` : ``;
            let result9 = result.queryAll[1] ? `<li class="list-group-item"> <label>Result9: </label> ${makeBadges(result.queryAll[1],20)} </li>` : ``;
            let result10 = result.queryAll[2] ? `<li class="list-group-item"> <label>Result10: </label> ${makeBadges(result.queryAll[2],20)} </li>` : ``;
            let result20 = result.queryAll[3] ? `<li class="list-group-item"> <label>Result20: </label> ${makeBadges(result.queryAll[3],20)} </li>` : ``;
            resultContent.innerHTML = `<div class='alert alert-success'> 
                <li class="list-group-item"> Insert result: <b></b> ${JSON.stringify(result.lastedResult)} </li>
                <li class="list-group-item"> <label>Last 20 record: </label> ${makeBadges(result.results, 20)} </li>
                ${result20}
                ${result10}
                ${result9}
                ${result8}
            </div>`;
        }

        function onError(error) {
            resultContent.innerHTML = `<div class='alert alert-danger'> ${error}</div>`;
        }

        submitBtn0.addEventListener("click", function() {
            disableBtns(true);
            pushData(0).then(onSuccess, onError).finally(disableBtns(false));
        })

        submitBtn1.addEventListener("click", () => {
            disableBtns(true);
            pushData(1).then(onSuccess, onError).finally(disableBtns(false));
        })

        function onPredictError(error) {
            predictContent.innerHTML = `<div class='alert alert-danger'> Predict fail with error: ${error}</div>`;
        }

        function onPredictSuccess(data) {
            predictContent.innerHTML = `<div class='alert alert-success' style="margin-top:20px;color:#FFF"> 
            <span class="list-group-item" style="background-color:#e54304">Table: <span class="badge ${badges[0]}">${data.table}</span></span>
            <span class="list-group-item" style="background-color:#e54304">Keyword: <span class="badge ${badges[0]}">${data.keyword}</span></span>
            <span class="list-group-item" style="background-color:#ee6002">Found records: <span class="badge ${badges[1]}">${data.result_rows}</span></span>
            <span class="list-group-item" style="background-color:#f47100">Count-0: <span class="badge ${badges[2]}">${data.count_0}</span></span>
            <span class="list-group-item" style="background-color:#fa8100">Count-1: <span class="badge ${badges[3]}">${data.count_1}</span></span>
            <span class="list-group-item" style="background-color:#ff8d00">Percent-0: <span class="badge ${badges[4]}">${data.percent_0.toFixed(4)} %</span></span>
            <span class="list-group-item" style="background-color:#ff9e22">Percent-1: <span class="badge ${badges[5]}">${data.percent_1.toFixed(4)} %</span></span>
            <div class="alert alert-secondary predict-result-content" >
            ${JSON.stringify(data.result_found)}
            </div>
            </div>`
        }

        $('.predict').on('click', function(e) {
            let sample = $("#sample-input").val();
            let table = e.currentTarget.getAttribute("table-data");
            if (table) {
                $.ajax({
                    url: "/predict",
                    method: "POST",
                    data: { value: table, input: sample },
                    success: function(data, status, jqXHR) {
                        if (jqXHR.status == 200) {
                            onPredictSuccess(data);
                        } else {
                            onPredictError("Request fail")
                        }
                    },
                    error: onPredictError

                })
            }
        })

    }
    /////