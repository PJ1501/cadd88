const HOST_NAME = "http://cadd-88.com"
const LOGIN_URL = "/process/login_ok.asp"
const GAME_INFOR = "/get_game_dadol_new.asp"

const USER_NAME = "test07";
const PASS = "123456"

const request = require("request");
let cookies = [];

function getUrl(url) {
    return HOST_NAME + url;
}
/**
 * Login by default username password
 */
module.exports.RequestLogin = function () {
    return new Promise((resolve, reject) => {
        request.post(getUrl(LOGIN_URL), {
            form: {
                mb_id: USER_NAME,
                mb_pass: PASS
            }
        }, (err, res, body) => {
            if (err) {
                reject(err);
            }
            if (res.statusCode === 200) {
                cookies = res.headers["set-cookie"];
                resolve(body);
            }
        })
    })

}

module.exports.GetGameDodal = function () {
    return new Promise((resolve, reject) => {
        let options = {
            url: getUrl(GAME_INFOR),
            method: "GET",
            headers: {
                "Cookie": cookies
            }
        }
        request.get(options, (err, res, body) => {
            if (err)
                reject(err);
            if (res.statusCode === 200) {
                if (body === "LOGOUT") {
                    module.exports.RequestLogin().then(
                        (data) => {
                            if (data === "YES") {
                                module.exports.GetGameDodal().then((value) => {
                                    resolve(value);
                                }, (error) => {
                                    reject(error);
                                })
                            } else
                                reject("can not login");
                        }, (err) => {
                            reject("can not login");
                        }
                    )
                } else {
                    resolve(body);
                }
            }
        })


    })
}