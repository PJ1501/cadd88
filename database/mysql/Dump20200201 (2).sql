CREATE DATABASE  IF NOT EXISTS `cadd88` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `cadd88`;
-- MySQL dump 10.13  Distrib 8.0.19, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: cadd88
-- ------------------------------------------------------
-- Server version	8.0.19

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `result_betting`
--

DROP TABLE IF EXISTS `result_betting`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `result_betting` (
  `id` int NOT NULL AUTO_INCREMENT,
  `result` tinyint DEFAULT NULL,
  `user_id` bigint DEFAULT NULL,
  `date_time` datetime DEFAULT NULL,
  `count_8` int DEFAULT NULL,
  `count_9` int DEFAULT NULL,
  `count_10` int DEFAULT NULL,
  `count_20` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `result_betting`
--

LOCK TABLES `result_betting` WRITE;
/*!40000 ALTER TABLE `result_betting` DISABLE KEYS */;
INSERT INTO `result_betting` VALUES (1,0,123,'2020-02-01 03:04:19',0,NULL,NULL,NULL),(2,1,123,'2020-02-01 03:04:20',1,NULL,NULL,NULL),(3,1,123,'2020-02-01 03:04:20',2,NULL,NULL,NULL),(4,1,123,'2020-02-01 03:04:21',3,NULL,NULL,NULL),(5,1,123,'2020-02-01 03:04:21',4,NULL,NULL,NULL),(6,1,123,'2020-02-01 03:04:22',5,NULL,NULL,NULL),(7,0,123,'2020-02-01 03:04:23',6,NULL,NULL,NULL),(8,0,123,'2020-02-01 03:04:23',7,NULL,NULL,NULL),(9,0,123,'2020-02-01 03:04:53',0,NULL,NULL,NULL),(10,1,123,'2020-02-01 03:04:54',1,NULL,NULL,NULL),(11,0,123,'2020-02-01 03:04:54',2,NULL,NULL,NULL),(12,0,123,'2020-02-01 03:04:55',3,NULL,NULL,NULL),(13,1,123,'2020-02-01 03:04:55',4,NULL,NULL,NULL),(14,0,123,'2020-02-01 03:04:56',5,NULL,NULL,NULL),(15,0,123,'2020-02-01 03:04:57',6,NULL,NULL,NULL),(16,1,123,'2020-02-01 03:04:58',7,NULL,NULL,NULL);
/*!40000 ALTER TABLE `result_betting` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `result_betting_BEFORE_INSERT` BEFORE INSERT ON `result_betting` FOR EACH ROW BEGIN
set @count8 = null;
SELECT 
    `result_betting`.count_8
INTO @count8 FROM
    `result_betting`
WHERE
    user_id = NEW.user_id
ORDER BY id DESC
LIMIT 1;
if @count8 = 6
then 
begin
set @c = (select group_concat(T0.result order by T0.id asc SEPARATOR '') from 
 (select * from result_betting where user_id=NEW.user_id order by id desc limit 7) as T0);
insert into result_betting_8 set user_id = NEW.user_id, result = concat(@c,NEW.result);
set NEW.count_8 = @count8+1;
end;
elseif @count8 is null or @count8 = 7 then
set NEW.count_8 = 0;
else
set NEW.count_8 = @count8+1;
end if;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `result_betting_8`
--

DROP TABLE IF EXISTS `result_betting_8`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `result_betting_8` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` bigint DEFAULT NULL,
  `result` varchar(10) NOT NULL,
  PRIMARY KEY (`id`,`result`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `result_betting_8`
--

LOCK TABLES `result_betting_8` WRITE;
/*!40000 ALTER TABLE `result_betting_8` DISABLE KEYS */;
INSERT INTO `result_betting_8` VALUES (1,123,'01111100'),(2,123,'01001001');
/*!40000 ALTER TABLE `result_betting_8` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'cadd88'
--

--
-- Dumping routines for database 'cadd88'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-02-01  3:10:00
