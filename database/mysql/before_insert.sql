CREATE DEFINER=`root`@`localhost` TRIGGER `result_betting_BEFORE_INSERT` BEFORE INSERT ON `result_betting` FOR EACH ROW BEGIN
set @count8 = null;
SELECT 
    `result_betting`.count_8
INTO @count8 FROM
    `result_betting`
WHERE
    user_id = NEW.user_id
ORDER BY id DESC
LIMIT 1;
if @count8 = 6
then 
begin
set @c = (select group_concat(T0.result order by T0.id asc SEPARATOR '') from 
 (select * from result_betting where user_id=NEW.user_id order by id desc limit 7) as T0);
insert into result_betting_8 set user_id = NEW.user_id, result = concat(@c,NEW.result);
set NEW.count_8 = @count8+1;
end;
elseif @count8 is null or @count8 = 7 then
set NEW.count_8 = 0;
else
set NEW.count_8 = @count8+1;
end if;
END