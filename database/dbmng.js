const mysql = require('mysql');
const mongoose = require("mongoose");
const logger = require("../utils/helper").Logger();
let MySQLPool;
//#region MYSQL
/**
 * @description Init MySQL pool connection first
 */
module.exports.initMySQL = function() {
    MySQLPool = mysql.createPool({
        connectionLimit: 10,
        host: 'localhost',
        user: 'root',
        password: '123456',
        database: 'cadd88'
    });
}

/**
 * @description Do Query mysql
 * 
 */
//todo: currently make simple for prototype, make advance later
module.exports.query = function(queryStr, values, options) {
        return new Promise((resolve, reject) => {
            if (!MySQLPool) {
                reject("Pool connection can not make");
            }
            MySQLPool.getConnection((error, connection) => {
                if (error) {
                    reject("Pool get connection error with message: " + error.message);
                } else {
                    connection.query(queryStr, values, (error, results, fields) => {
                        connection.release();
                        if (error) {
                            reject(`Query ${queryStr} throught error: ${error.message}`);
                        } else {
                            resolve([results, fields]);
                        }

                    })
                }
            })
        });
    }
    /**
     *@description Insert raw value without mysql escape
     * @param {*} value 
     */
module.exports.rawValue = function(value) {
        return mysql.raw(value);
    }
    //#endregion

//#region MONGODB
/**
 * @description init mongodb with static config internal
 */
module.exports.initMongoDB = function() {
    registerStatusCheck();
    return mongoose.connect("mongodb://localhost/gkr", {
        poolSize: 6,
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useFindAndModify: false
    }).catch((error) => {
        logger.error("Connect to mongodb error with message " + error);
    });
}
let mongoStatus = mongoose.Connection.STATES.uninitialized;
////#region  Define Schema and Model
const Schema = mongoose.Schema;
const ResultBettingSchema = new Schema({
    result: Number,
    date_time: {
        type: Date,
        default: Date.now
    },
    index: Number
}, {
    collection: "result_betting"
});
let ResultBetting = mongoose.model("result_betting", ResultBettingSchema);

const CounterSchema = new Schema({
    "_id": String,
    "sequence_value": Number
}, {
    collection: "counter"
});
let CounterModel = mongoose.model("counter", CounterSchema);
const schemaDefine = {
    "date_time": {
        type: Date,
        default: Date.now
    },
    "result": String
};
const Result8Schema = new Schema(schemaDefine, { collection: "result_betting_8" });
const Result9Schema = new Schema(schemaDefine, { collection: "result_betting_9" });
const Result10Schema = new Schema(schemaDefine, { collection: "result_betting_10" });
const Result20Schema = new Schema(schemaDefine, { collection: "result_betting_20" });
let Result8Model = mongoose.model("restult8", Result8Schema);
let Result9Model = mongoose.model("restult9", Result9Schema);
let Result10Model = mongoose.model("restult10", Result10Schema);
let Result20Model = mongoose.model("restult20", Result20Schema);

//#endregion
function registerStatusCheck() {
    mongoose.connection.on("connected", () => {
        mongoStatus = mongoose.Connection.STATES.connected;
    })
    mongoose.connection.on("error", function(err) {
        mongoStaus = mongoose.Connection.STATES.uninitialized;
    })

    mongoose.connection.on("disconnected", function() {
        mongoStaus = mongoose.Connection.STATES.disconnected;
    })
}

module.exports.mongoStaus = function() {
    return mongoStatus;
}

module.exports.mongoReady = function() {
    return mongoStatus == mongoose.Connection.STATES.connected;
}

module.exports.insertResult = async function(value) {
    //return object
    let res = { lastedResult: undefined, results: undefined, queryAll: [] };
    //insert into db first
    let insertResult = await new ResultBetting(value).save();
    if (insertResult.errors)
        return Promise.reject("Insert error " + insertResult.errors);
    //store insert result to return
    res.lastedResult = insertResult.toObject();
    console.log(insertResult);
    //find last 20 records
    let queryAll = await ResultBetting.find().sort({ date_time: -1 }).limit(20).exec();
    if (queryAll.length > 0) {
        let results = [];
        for (let i = queryAll.length - 1; i >= 0; i--) {
            results.push(queryAll[i].result);
        }
        let resultStr = results.join('');
        res.results = resultStr;
        let models = [];
        if (resultStr.length >= 8) {
            res.queryAll[0] = resultStr.substr(resultStr.length - 8, 8);
            models.push(new Result8Model({ result: res.queryAll[0] }).save());
        }
        if (resultStr.length >= 9) {
            res.queryAll[1] = resultStr.substr(resultStr.length - 9, 9);
            models.push(new Result9Model({ result: res.queryAll[1] }).save());
        }
        if (resultStr.length >= 10) {
            res.queryAll[2] = resultStr.substr(resultStr.length - 10, 10);
            models.push(new Result10Model({ result: res.queryAll[2] }).save());
        }
        if (resultStr.length == 20) {
            res.queryAll[3] = resultStr;
            models.push(new Result20Model({ result: res.queryAll[3] }).save());
        }
        return Promise.all(models).then(() => { return Promise.resolve(res) }, (err) => { Promise.reject(err) });
        //return Promise.resolve(res);
    } else {
        return Promise.resolve(res);
    }
}
module.exports.insertResultv2 = function(value) {
    let lastResult;
    return new ResultBetting(value).save().
    then((r) => {
        lastResult = r;
        return ResultBetting.find().sort({
            date_time: -1
        }).limit(8);
    }).then((rows) => {
        if (rows.length == 8) {
            let result = [];
            for (let i = 7; i >= 0; i--) {
                result.push(rows[i].result);
            }
            console.log(result.join(''));
            return new Result8Model({
                    result8: result.join('')
                }).save()
                .then(() => {
                    return lastResult
                });
        } else {
            return (lastResult);
        }
    })
}

module.exports.predict = function(value) {
    return ResultBetting.find().sort({
        date_time: -1
    }).limit(7).
    then((rows) => {
        let result = [];
        for (let i = 6; i >= 0; i--) {
            result.push(rows[i].result);
        }
        console.log(result.join(''));
        return Result8Model.find({
            "result8": new RegExp('^' + result.join('') + '.$')
        }, {
            "_id": 0,
            result8: 1
        }).then((rows) => {
            let totalRows = rows.length;
            let count0 = 0;
            let count1 = 0;
            rows.forEach((item) => {
                if (item.result8.charAt(item.result8.length - 1) == 0)
                    count0 += 1;
                else count1 += 1
            })
            return {
                keyword: result.join(''),
                result_rows: rows.length,
                count_0: count0,
                count_1: count1,
                percent_0: (count0 / totalRows) * 100,
                percent_1: (count1 / totalRows) * 100,
                result_found: rows,

            }
        });
    });
}

module.exports.predictv2 = async function(value, input) {
        if (![8, 9, 10, 20].includes(value))
            return Promise.reject("invalid value");
        let keyword;
        if (input == undefined || input == "") {
            let result = await ResultBetting.find().sort({ date_time: -1 }).limit(value - 1).exec();
            if (result.errors)
                return Promise.reject("Query error " + insertResult.errors);
            let keywordArr = [];
            for (let i = value - 2; i >= 0; i--) {
                keywordArr.push(result[i].result);
            }
            console.log(keywordArr.join(''));
            keyword = keywordArr.join('');
        } else
            keyword = input;

        let rows;
        switch (value - 1) {
            case 7:
                rows = await Result8Model.find({ "result": new RegExp('^' + keyword + '.$') }, { "_id": 0, result: 1 }).exec();
                break;
            case 8:
                rows = await Result9Model.find({ "result": new RegExp('^' + keyword + '.$') }, { "_id": 0, result: 1 }).exec();
                break;
            case 9:
                rows = await Result10Model.find({ "result": new RegExp('^' + keyword + '.$') }, { "_id": 0, result: 1 }).exec();
                break;
            case 19:
                rows = await Result20Model.find({ "result": new RegExp('^' + keyword + '.$') }, { "_id": 0, result: 1 }).exec();
                break;
        }
        let totalRows = rows.length;
        let count0 = 0;
        let count1 = 0;

        if (rows.length > 0) {
            rows.forEach((item) => {
                if (item.result.charAt(item.result.length - 1) == 0)
                    count0 += 1;
                else count1 += 1
            })
        }
        let res = {
            table: "Result_" + value,
            keyword: keyword,
            result_rows: rows.length,
            count_0: count0,
            count_1: count1,
            percent_0: count0 > 0 || count1 > 0 ? (count0 / totalRows) * 100 : 50,
            percent_1: count0 > 0 || count1 > 0 ? (count1 / totalRows) * 100 : 50,
            result_found: rows,

        };
        return Promise.resolve(res);
    }
    //#endregion