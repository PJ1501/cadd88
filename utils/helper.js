const winston = require("winston");
require('dotenv').config();

function MyLogger(location) {
    this.location = location;
    this.logger = winston.createLogger({
        transports: [new winston.transports.Console()]
    });
}

MyLogger.prototype.log = function () {
    // this.logger.info("aaaaaa");
    // this.logger.error("fffffff");
    if (process.env.DEBUG == 0) {
        console.log(arguments[0]);
    } else {
        this.logger.log({
            level: "info",
            message: arguments[0]
        })
    }
    // console.log(123);
}

MyLogger.prototype.error = function () {
    if (process.env.DEBUG == 0) {
        console.log(arguments[0]);
    } else {
        this.logger.error(arguments[0]);
    }
}

module.exports.Logger = function (location) {
    return new MyLogger(location);
}