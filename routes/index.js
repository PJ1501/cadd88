var express = require('express');
var router = express.Router();
// const http = require('http');
const controller = require("../controllers/main.controller");
const logger = require("../utils/helper").Logger("Router/index.js");
const Schema = require("../database/Schema").Schema;
const db = require("../database/dbmng");

/* GET home page. */
router.get('/', function(req, res, next) {
    res.render('index', {
        title: '캔디',
        user_name: "---",
        win_count: 0,
        user_money: 0,
        user_point: 0,
        memo_count: 0
    });
});

router.get("/login", function(req, res, next) {
    logger.log("Heeeellllooooo -" + Date.now());
    res.send("zzzzz" + Date.now());
    // controller.RequestLogin().then(
    //     (data) => {
    //         res.send(data);
    //     },
    //     (e) => {
    //         res.send(e);
    //     }).catch(reason => {

    //     console.log(reason);
    // })

});

router.get("/get_dadol_new", function(req, res, next) {
    controller.GetGameDodal().then((value) => {
        res.send(value);
    }, (error) => {
        res.send(error);
    }).catch(reason => {
        res.send(reason)
    });

})

router.get("/result", (req, res, next) => {
    res.render("result");
})

router.all("/testdb", function(req, res, next) {
    console.log(req.body);
    if (req.body.result && (req.body.result == 0 || req.body.result == 1)) {
        db.insertResult({
            // result: (Math.floor(Math.random() * 100) + 1) % 2
            result: parseInt(req.body.result)
        }).then(function(value) {
            res.json(value);
        }, function(err) {
            logger.error("Can not insert with error: " + err);
            res.json({ errors: err });
        })
    } else {
        next(createError(400));
    }
});



router.get("/predict", function(req, res, next) {
    db.predictv2(parseInt(req.query.value), req.query.input).then(value => {
        res.json(value);
    }, reason => {
        console.log(reason);
        res.send(reason);
    })
})
router.post("/predict", function(req, res, next) {
    db.predictv2(parseInt(req.body.value), req.body.input).then(value => {
        res.json(value);
    }, reason => {
        console.log(reason);
        res.json({ error: reason });
    })
})
module.exports = router;